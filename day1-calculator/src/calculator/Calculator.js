import React, { Component } from 'react';
import Button from './Button';
import * as math from 'mathjs';
import { Icon } from 'react-icons-kit';
import { iosArrowThinLeft } from 'react-icons-kit/ionicons/iosArrowThinLeft';
import './Calculator.css';

class Calculator extends Component {
  constructor() {
    super();
    this.state = {
      expressions: [],
      result: '0',
      buttons: [
        {
          label: 'C',
          theme: 'control',
          type: 'clear'
        },
        { label: '7', value:'7' },
        { label: '4', value:'4' },
        { label: '1', value:'1' },
        { label: '00', value:'00' },
        {
          label: <Icon size={48} icon={iosArrowThinLeft} />,
          theme: 'control',
          type: 'remove'
        },
        { label: '8', value:'8' },
        { label: '5', value:'5' },
        { label: '2', value:'2' },
        { label: '0', value:'0' },
        { label: '%', theme: 'control', value:'%' },
        { label: '9', value:'9' },
        { label: '6', value:'6' },
        { label: '3', value:'3' },
        { label: '.', value:'.' },
        { label: '/', theme: 'control', value:'/' },
        { label: 'x', theme: 'control', value:'*' },
        { label: '+', theme: 'control', value:'+' },
        { label: '-', theme: 'control', value:'-' },
        {
          label: '=',
          theme: 'equal',
          value:'equal',
          type: 'equal'
        }
      ]
    };
  }
  onButtonClick = (value) => {
    const { expressions } = this.state;
    expressions.push(value);
    this.setState({
      expressions
    });
  }
  onRemoveClick = () => {
    const { expressions } = this.state;
    expressions.pop();
    this.setState({
      expressions
    });
  }
  onClearClick = () => {
    this.setState({
      expressions: [],
      result: '0'
    });
  }
  onEqualClick = () => {
    this.calculateExpression();
  }
  calculateExpression = () => {
    const { expressions } = this.state;
    const expression = expressions.join('');
    try {
      console.log(expression);
      const result = expression ? String(math.eval(expression)) : '0';
      console.log(result);
      this.setState({
        result
      });
    } catch(ex) {
      console.log("Exception");
      console.log(ex);
    }
  }
  renderButtons = () => {
    const { buttons } = this.state;
    return buttons.map(({theme, label, value, type = ""}, index) => {
      if (type === "remove") {
        return (
          <Button
            key={index}
            onButtonClick={this.onRemoveClick}
            theme={theme}>{label}</Button>
        );
      } else if (type === "clear") {
        return (
          <Button
            key={index}
            onButtonClick={this.onClearClick}
            theme={theme}>{label}</Button>
        );
      } else if (type === "equal") {
        return (
          <Button
            key={index}
            onButtonClick={this.onEqualClick}
            theme={theme}>{label}</Button>
        );
      } else {
        return (
          <Button
            key={index}
            onButtonClick={this.onButtonClick.bind(this, value)}
            theme={theme}>{label}</Button>
        );
      }
    });
  }
  render() {
    const { expressions, result } = this.state;
    const expression = expressions.join('');
    return (
      <div className="calculator">
        <div className="display">
          <div className="expression">{expression}</div>
          <div className="result">{math.eval(result)}</div>
        </div>
        <div className="buttons">
          {this.renderButtons()}
        </div>
      </div>
    );
  }
}

export default Calculator;
