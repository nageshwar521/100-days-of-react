import React, { Component } from 'react';
import './Calculator.css';

class Button extends Component {
  onClick = () => {
    if (this.props.onButtonClick) {
      this.props.onButtonClick();
    }
  }
  render() {
    const { theme = "", children } = this.props;
    return (
      <div
        className={`button ${theme}`}
        onClick={this.onClick}>
        {children}
      </div>
    );
  }
}

export default Button;
