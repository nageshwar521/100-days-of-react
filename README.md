# 100 Days of React

> A collection of React apps built by [Nageshwar Reddy](https://about.me/nageshwar521)

## Days
<table>
    <thead>
        <tr>
            <th>Day</th>
            <th>Preview</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><strong>Day 1</strong></td>
            <td><img src="src/calculator/Calculator.png"></img></td>
            <td>React Calculator</td>
        </tr>
    </tbody>
</table>
